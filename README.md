# Clean Sandbox

A sandbox for the [Clean][] language running completely in the browser.

The setup is as follows:

- A simple `clm`/`cpm`-like make tool (written in Clean) runs in the
  [WebAssembly interpreter][interpreter].

- The compiler frontend (written in Clean) runs in the interpreter as well.

- The compiler backend (written in C) is compiled using [emscripten][]. We
  catch interpreter errors due to `ccall`s and file I/O instructions (which are
  not implemented in the default JavaScript runtime), and link these to the
  emscripten-compiled C code (which also runs in WebAssembly, but in a
  different part of the memory).

- The bytecode generator and bytecode prelinker of the interpreter (written in
  C) are compiled using emscripten and used as-is, with their own chunk of
  memory.

- We can run the prelinked bytecode in the WebAssembly interpreter, exactly the
  same way as the make tool and the compiler frontend are executed.

## About `ccall`s and file I/O

In the future, `ccall` and file I/O support may be added to the standard
JavaScript supporting code of the WebAssembly interpreter. There are some
caveats however.

Currently, only the following types are supported:

- For arguments: `IpS`.
- For return types: `VIpS`.
- 'State' types (the optional third list of types) are fully supported, as they
  can be ignored by this runtime system.
- 'Flags' (`GP` at the start) are ignored.

There are also a few differences with the normal C ffi:

- The Clean heap and the C heap are strictly separated. This means that when a
  pointer is passed to C (e.g. with a `S`, `s`, or `A` argument type), the C
  function may modify the memory at that address, but changes will not be
  reflected on the Clean side.

- Because of the same issue, the C function may read beyond the bounds of
  strings and arrays passed to it but will not read anything meaningful.

- Note that emscripten is a 32-bit target. C code must be compatible with that.
  C code may [detect emscripten in the preprocessor][emscripten-detect].

[Clean]: https://clean-lang.org/
[emscripten]: https://emscripten.org/
[emscripten-detect]: https://emscripten.org/docs/compiling/Building-Projects.html#detecting-emscripten-in-preprocessor
[interpreter]: https://gitlab.com/clean-and-itasks/abc-interpreter/
