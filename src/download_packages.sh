#!/bin/bash

set -e

ALLOWED_TARGETS='[."linux-x64",."linux-any",."linux-intel",."any-any",."any-intel"]'
PACKAGES="
base-lib
base-stdenv
base64
bible-references
category-theory
clean-platform
clean-doc
clean-types
containers
gast
generic-diff
generic-print
gentype
graphviz
html
json
language
options
parser-combinators
random
regex
spdx-licenses
test
text
yaml
"

JQ_SIMPLIFY_REGISTRY='
reduce .[] as $p
	( {}
	; . + {($p.name) : ($p.versions | map_values(.targets | map_values(.url)))}
	)
'
JQ_GET_URLS="map_values([$ALLOWED_TARGETS] | .[][] | select(. != null)) | to_entries | map(.key,.value) | .[]"

REGISTRY="$(curl -s https://clean-lang.org/api/packages | jq "$JQ_SIMPLIFY_REGISTRY")"

mkdir -p data/packages

exec 4> >(jq -sc 'reduce .[] as $p ({}; . + $p)' > data/packages.json)

for PKG in $PACKAGES; do
	echo "$REGISTRY" | jq -rc "{\"$PKG\": (.\"$PKG\" | keys_unsorted)}" >&4

	exec 5< <(echo "$REGISTRY" | jq -r ".\"$PKG\" | $JQ_GET_URLS")
	while read VERSION <&5; do
		read URL <&5
		echo "$PKG-$VERSION: $URL"
		(cd data/packages
			mkdir $PKG-$VERSION
			curl -# "$URL" | tar xz --strip-components=1 -C $PKG-$VERSION
			tar czf $PKG-$VERSION.tar.gz $PKG-$VERSION
			rm -r $PKG-$VERSION)
	done
done

exec 4>&-
