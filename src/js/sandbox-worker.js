"use strict";

let window=globalThis; // fix to get js-untar to work in a web worker
importScripts (
	'instructions.js',
	'compiler.js',
	'bcgen.js',
	'bclink.js',
	'bcprelink.js',
	'abc-interpreter.js',
	'../node_modules/text-encoding/lib/encoding.js',
);

let c_compiler=null;
let c_bcgen=null;
let c_bclink=null;
let c_bcprelink=null;

/* Monkey-patch fetch() to keep a cache */
(() => {
	const org_fetch=globalThis.fetch;
	const cache=new Map();

	globalThis.fetch=path => {
		// Support deployments in directories instead of on the absolute root
		if (path[0]=='/')
			path='..'+path;

		if (cache.has (path))
			return new Promise (resolve => resolve (cache.get (path)));

		return org_fetch (path).then (response => {
			if (response.ok){
				cache.set (path,response);
				response.org_arrayBuffer=response.arrayBuffer;
				response.arrayBuffer=() => {
					var buffer=null;
					var resolve=null;
					var first_read_finished=new Promise (r => {resolve=r;});
					response.arrayBuffer=() => first_read_finished.then (() => buffer);
					return response.org_arrayBuffer().then (b => {
						buffer=b;
						resolve();
						return b;
					});
				};
				return response;
			} else {
				throw 'failed to fetch '+path;
			}
		});
	};
})();

const sandbox={
	shared_clean_values: null,

	/* This will be set by sandbox.icl */
	compile: () => console.error ('the Clean backend has not loaded yet!'),

	stdout: (s) => { postMessage ({stdout: s}); },
	stderr: (s) => { postMessage ({stderr: s}); },

	init: () => {
		const make_tool=ABCInterpreter.instantiate({
			bytecode_path: '/data/sandbox.pbc',

			heap_size: 2<<20,
			stack_size: 512<<10,

			encoding: 'utf-8',

			with_js_ffi: true,
			js_ffi_options: {}
		}).then (abc => {
			abc.log=s => {postMessage ({stdout: s})};
			const asp=abc.interpreter.instance.exports.get_asp();
			const hp=abc.interpreter.instance.exports.get_hp();
			const hp_free=abc.interpreter.instance.exports.get_hp_free();

			const start_node=abc.memory_array[(abc.start>>2)+2];
			abc.memory_array[hp>>2]=start_node;
			abc.memory_array[(hp>>2)+1]=0;
			abc.interpreter.instance.exports.set_hp (hp+24);
			abc.interpreter.instance.exports.set_hp_free (hp_free-3);

			const index=abc.share_clean_value (hp,sandbox);

			abc.memory_array[asp>>2]=hp;
			abc.memory_array[(asp>>2)+1]=0;
			abc.interpreter.instance.exports.set_pc (start_node);

			const csp=abc.interpreter.instance.exports.get_csp();
			abc.memory_array[csp/4]=658*8; // instruction 0; to return
			abc.interpreter.instance.exports.set_csp (csp+8);

			abc.interpreter.instance.exports.interpret();

			abc.interpret (new SharedCleanValue (index),[sandbox,1]);
		});

		return Promise.all ([init_emscripten_modules(), make_tool])
				.then (() => {
					postMessage ({init_done: {ok: true}});
				}).catch (e => {
					postMessage ({init_done: {ok: false, error: e.message}});
				});
	},

	add_package: (pkg, v, files) => {
		files.forEach (f => {
			if (f.type != '0') /* normal file */
				return;

			let path='/clean/lib';
			const parts=f.name.split ('/');
			parts.pop();
			while (parts.length>0){
				path+='/'+parts.shift();
				try {
					c_compiler.FS.mkdir (path);
				} catch (e) {}
			}
			c_compiler.FS.writeFile ('/clean/lib/'+f.name,new Uint8Array (f.buffer));
		});
		return new Promise (resolve => c_compiler.FS.syncfs (false,resolve));
	},

	run: (path) => {
		const opts={
			fetch: (p) => {
				return p!==null ? fetch (p) : new Promise ((resolve) => {
					const pbc=c_bcprelink.FS.readFile (path);
					resolve ({
						ok: true,
						arrayBuffer: () => pbc.buffer
					});
				});
			},
			interpreter_imports: {
				halt: () => {return;}
			}
		};

		return ABCInterpreter.instantiate (opts).then ((abc) => {
			abc.log=sandbox.stdout;

			abc.interpreter.instance.exports.set_pc(abc.start);
			var r=0;
			try {
				r=abc.interpreter.instance.exports.interpret();
			} catch (e) {
				sandbox.stderr (e+'\n');
				r=-1;
			}

			postMessage ({flush_output: {}});

			if (r!=0)
				sandbox.stderr ('failed with return code '+r);
		});
	}
};

onmessage=function (e) {
	e=e.data;
	if ('init' in e)
		sandbox.init();
	if ('add_package' in e){
		sandbox.add_package (e.add_package.name,e.add_package.version,e.add_package.files)
				.then (() => {
					postMessage ({add_package_done: {ok: true}});
				}).catch (e => {
					postMessage ({add_package_done: {ok: false, error: e.message}});
				});
	}
	if ('compile' in e)
		c_compiler.FS.syncfs (true,() => {
			c_compiler.FS.writeFile ('/clean/src/sandbox.icl',e.compile.module);
			sandbox.compile (e.compile.paths,'sandbox',ok => {
				postMessage ({compile_done: {ok: ok}});
			});
		});
	if ('run' in e)
		sandbox.run ('sandbox.pbc');
};

function fmtime (path)
{
	try {
		return this.stat (path).mtime.getTime();
	} catch (e) {}
	return 0;
}

function read_abc_dependencies (path)
{
	try {
		const arr=c_compiler.FS.readFile (path);
		const decoder=new TextDecoder();
		const deps=[];

		var start=0;
		while (start<arr.length){
			for (var end=start; end<arr.length && arr[end]!=10; end++);
			const s=decoder.decode (new Uint8Array (arr.buffer,start,end-start));
			if (s.substring (0,7)=='.depend')
				deps.push (s.substring (9,s.length-1));
			else if (s=='.endinfo')
				break;
			start=end+1;
		}

		return deps;
	} catch (e) {console.warn (e);}

	return [];
}

let clean_compiler=null;

function create_clean_compiler ()
{
	return ABCInterpreter.instantiate ({
		bytecode_path: '/data/cocl.pbc',
		heap_size: 128<<20,
		stack_size: 2<<20,

		interpreter_imports: {
			handle_illegal_instr: (pc,instr,asp,bsp,csp,hp,hp_free) => {
				instr=ABCInterpreter.instructions[instr];
				if (instr=='ccall')
					return i_ccall.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='closeF')
					return i_closeF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='endF')
					return i_endF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='openF')
					return i_openF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='readLineF')
					return i_readLineF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='stderrF')
					return i_stderrF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='stdioF')
					return i_stdioF.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='writeFC')
					return i_writeFC.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='writeFI')
					return i_writeFI.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else if (instr=='writeFS')
					return i_writeFS.bind (c_compiler) (clean_compiler,pc,asp,bsp);
				else
					return 0;
			},
			illegal_instr: function (addr, instr) {
				const name=ABCInterpreter.instructions[instr];
				addr=addr/8-clean_compiler.code_offset;
				throw 'illegal instruction '+instr+' ('+name+') at address '+addr;
			},
			out_of_memory: function () {
				throw 'out of memory';
			},
			halt: function (pc, hp_free, hp_size) {
			},

			putchar: function (v) {
				if (clean_compiler.skip_next_newline){
					clean_compiler.skip_next_newline=false;
					if (v==10)
						return;
					else
						sandbox.stdout (65536);
				}
				sandbox.stdout (String.fromCharCode(v));
			},
			print_int: function (high,low) {
				if ((high==0 && low>=0) || (high==-1 && low<0)){
					if (low==65536){
						clean_compiler.skip_next_newline=true;
						return;
					}
					sandbox.stdout (low);
				} else if (typeof BigInt!='undefined'){
					var n=BigInt(high)*BigInt(2)**BigInt(32);
					if (low<0) {
						n+=BigInt(2)**BigInt(31);
						low+=2**31;
					}
					n+=BigInt(low);
					sandbox.stdout (n);
				} else {
					console.warn ('print_int: truncating 64-bit integer because this browser has no BigInt');
					sandbox.stdout (low);
				}
			},
			print_bool: function (v) {
				sandbox.stdout (v==0 ? 'False' : 'True');
			},
			print_char: function (v) {
				sandbox.stdout ("'"+String.fromCharCode(v)+"'");
			},
			print_real: function (v) {
				sandbox.stdout (Number(0+v).toLocaleString(
					['en-US'],
					{
						useGrouping: false,
						maximumSignificantDigits: 15,
					}
				));
			},
		}
	}).then (abc => {
		clean_compiler=abc;

		// hack to not show '65536' after every run
		clean_compiler.skip_next_newline=false;

		clean_compiler.files=[undefined,undefined]; /* first two are stdio and stderr */
		clean_compiler.stdio_open=false;

		clean_compiler.clear_cafs=function(){
			var p=97*2;
			while (p!=0){
				var next=clean_compiler.memory_array[p-2]>>2;
				clean_compiler.memory_array[p-2]=0;
				clean_compiler.memory_array[p-1]=0;
				clean_compiler.memory_array[p  ]=0;
				clean_compiler.memory_array[p+1]=0;
				p=next;
			}
			clean_compiler.memory_array[97*2]=97*8;
		};

		clean_compiler.compile=function(){
			clean_compiler.clear_cafs();
			c_compiler.clean_stdio_open=false;

			var res=0;
			try {
				clean_compiler.interpreter.instance.exports.set_pc (clean_compiler.start);
				clean_compiler.interpreter.instance.exports.interpret()==0;
				res=c_compiler.clean_return_code;
			} catch (e) {
				console.warn (e);
				res=e;
				/* Memory may be badly corrupted, create a new instance to be sure */
				create_clean_compiler();
			}

			postMessage ({flush_output: {}});

			return res;
		};
	});
}

function clean_call_main (argc,argv)
{
	try {
		return this._main (argc,argv);
	} catch (e) {
		console.warn (e);
		throw e;
		return -1;
	}
}

function init_emscripten_modules ()
{
	const instantiate_opts={
		print: (s) => sandbox.stdout (s+'\n'),
		printErr: (s) => sandbox.stderr (s+'\n')
	};

	return compiler (Object.assign ({},instantiate_opts)).then ((instance) => {
		c_compiler=instance;
		c_compiler.clean_stdout=sandbox.stdout;
		c_compiler.clean_stderr=sandbox.stderr;

		// ArgEnv
		c_compiler._ArgEnvGetCommandLineCountC=() => c_compiler.clean_argc;

		c_compiler._ArgEnvGetCommandLineArgumentC=(i,sizep,sp) => {
			const arg=c_compiler.HEAP32[c_compiler.clean_argv/4+i];
			var p=arg;
			for (; c_compiler.HEAPU8[p]!=0; p++);
			c_compiler.HEAP32[sizep/4]=p-arg;
			c_compiler.HEAP32[sp/4]=arg;
		};

		c_compiler._ArgEnvCopyCStringToCleanStringC=(cs,cleans) => {
			const size=c_compiler.HEAP32[cleans/4];
			cleans+=4;
			for (var i=0; i<size; i++)
				c_compiler.HEAPU8[cleans++]=c_compiler.HEAPU8[cs++];
		};
		c_compiler._ArgEnvCopyCStringToCleanStringC.sync_strings_back_to_clean=true;

		// set_return_code
		c_compiler._set_return_code=(c) => {c_compiler.clean_return_code=c;};
	}).then (() => Promise.all ([
		create_clean_compiler(),
		bcgen     (Object.assign ({},instantiate_opts)).then ((instance) => c_bcgen=instance),
		bclink    (Object.assign ({},instantiate_opts)).then ((instance) => c_bclink=instance),
		bcprelink (Object.assign ({},instantiate_opts)).then ((instance) => c_bcprelink=instance)
	])).then (() => {
		c_bcgen.clean_call_main=clean_call_main.bind (c_bcgen);
		c_bclink.clean_call_main=clean_call_main.bind (c_bclink);
		c_bcprelink.clean_call_main=clean_call_main.bind (c_bcprelink);

		c_compiler.FS.mkdir ('/clean');
		c_compiler.FS.mount (c_compiler.IDBFS,{},'/clean');
		c_compiler.FS.mkdir ('/clean/lib');
		c_compiler.FS.mkdir ('/clean/src');
		c_compiler.FS.fmtime=fmtime.bind (c_compiler.FS);

		const mount=backend => new Promise (resolve => {
			backend.FS.mkdir ('/clean');
			backend.FS.mount (backend.IDBFS,{},'/clean');
			backend.FS.syncfs (true,err => {
				if (err)
					throw err;
				backend.FS.chdir ('/clean/src');
				resolve();
			});
			backend.FS.fmtime=fmtime.bind (backend.FS);
		});

		return new Promise (resolve => {
			c_compiler.FS.syncfs (false,() => {
				Promise.all ([
					mount (c_bcgen),
					mount (c_bclink),
					mount (c_bcprelink),
				]).then (resolve);
			});
		});
	});
}
