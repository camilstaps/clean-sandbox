"use strict";

function escapeHTML (unsafe)
{
	const map={
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};
	return String (unsafe).replace (/[&<>"'\/]/g, (s) => map[s]);
}

const sandbox={
	worker: null,

	output_timer: null,
	output_buffer: '',
	flush_output: () => {
		if (sandbox.output_timer){
			window.clearTimeout (sandbox.output_timer);
			sandbox.output_timer=null;
		}

		const at_bottom=sandbox.output.scrollTop+sandbox.output.offsetHeight>=sandbox.output.scrollHeight;
		sandbox.output.append (sandbox.output_buffer);
		sandbox.output_buffer='';
		if (at_bottom)
			sandbox.output.parentNode.scrollTop=sandbox.output.parentNode.scrollHeight;
	},
	stdout: (s) => {
		/* Add zero-width space to allow line wrapping on commas */
		s=String (s).replace (/,/g,',\u200b');
		sandbox.output_buffer+=s;
		if (s.indexOf ('\n')>=0)
			/* Flush immediately on line endings */
			sandbox.flush_output();
		else if (sandbox.output_timer===null)
			/* We use an interval to not spend all our time in layout */
			sandbox.output_timer=window.setTimeout (sandbox.flush_output,100);
	},
	stderr: (s) => {
		let span=document.createElement ('span');
		span.className='err';
		span.textContent=s;
		/* Stderr is less frequent than stdout, so we just write without timeout */
		sandbox.flush_output();
		sandbox.output.append (span);
	},

	update_package_views: () => {
		sandbox.packages_selection.innerHTML='';
		sandbox.packages_add_name.innerHTML='';
		sandbox.packages_add_button.disabled=true;

		for (let name in sandbox.packages){
			if (sandbox.packages[name].loaded !== null){
				let li=document.createElement ('li');
				li.innerText=name + ' ' + sandbox.packages[name].loaded;
				sandbox.packages_selection.appendChild (li);
			} else {
				let option=document.createElement ('option');
				option.value=name;
				option.innerText=name;
				sandbox.packages_add_name.appendChild (option);
				sandbox.packages_add_button.disabled=false;
			}
		};
		sandbox.packages_add_name.onchange(); /* update version field */
	},

	add_package_callback: null,
	fetch_package: (pkg, v) => {
		return new Promise ((resolve,reject) => {
					if (sandbox.packages[pkg].loaded !== null)
						reject ('This package has already been loaded.');
					sandbox.packages[pkg].loaded=v;
					sandbox.update_package_views();
					resolve();
				}).then (() => fetch ('data/packages/'+pkg+'-'+v+'.tar.gz'))
				.then (res => res.arrayBuffer())
				.then (pako.inflate)
				.then (arr => untar (arr.buffer))
				.then (files => new Promise ((resolve,reject) => {
					sandbox.add_package_callback=function (e) {
						sandbox.add_package_callback=null;
						if (e.ok)
							resolve();
						else
							reject (e.error);
					};

					sandbox.worker.postMessage ({add_package: {
						name: pkg,
						version: v,
						files: files
					}});
				}));
	},

	init: (opts) => {
		sandbox.btn_compile=opts.btn_compile;
		sandbox.btn_run=opts.btn_run;
		sandbox.compile_indicator=opts.compile_indicator;
		sandbox.output=opts.output;
		sandbox.packages_selection=opts.packages_selection;
		sandbox.packages_add_name=opts.packages_add_name;
		sandbox.packages_add_version=opts.packages_add_version;
		sandbox.packages_add_button=opts.packages_add_button;

		/* Interface */
		const ui=create_monaco_editor (opts.editor).then (editor => {
			sandbox.editor=editor;
			editor.setValue (`module sandbox

import StdEnv

Start = take 10 primes

primes =:
	[ 2, 3
	:[ p
	\\\\ p <- [5,7..]
	 | isEmpty [d \\\\ d <- takeWhile (\\d -> d*d <= p) primes | p rem d == 0]
	]]`);
		});

		sandbox.btn_compile.onclick=() => {
			sandbox.btn_compile.disabled=true;
			sandbox.compile_indicator.style.display='inline-block';
			sandbox.output.innerHTML='';

			let paths=['/clean/src'];
			for (let name in sandbox.packages)
				if (sandbox.packages[name].loaded !== null)
					paths.push ('/clean/lib/'+name+'-'+sandbox.packages[name].loaded+'/lib');

			sandbox.worker.postMessage ({compile: {
				module: sandbox.editor.getValue(),
				paths: paths
			}});
		};

		sandbox.btn_run.onclick=() => {
			sandbox.output.innerHTML='';
			sandbox.worker.postMessage ({run: {}});
		};

		sandbox.packages_add_name.onchange=() => {
			sandbox.packages_add_version.innerHTML='';
			let name=sandbox.packages_add_name.value;
			if (!name)
				return;
			sandbox.packages[name].forEach (version => {
				let option=document.createElement ('option');
				option.value=version;
				option.innerText=version;
				sandbox.packages_add_version.appendChild (option);
			});
			sandbox.packages_add_version.childNodes[sandbox.packages_add_version.childNodes.length-1].selected=true;
		};

		sandbox.packages_add_button.onclick=() => {
			let pkg=sandbox.packages_add_name.value;
			let v=sandbox.packages_add_version.value;

			/* disabled status of the button is handled in update_package_views() */
			sandbox.packages_add_name.disabled=true;
			sandbox.packages_add_version.disabled=true;

			sandbox.fetch_package (pkg,v)
					.catch (e => {
						window.alert (e);
					}).finally (() => {
						sandbox.packages_add_name.disabled=false;
						sandbox.packages_add_version.disabled=false;
					});
		};

		/* Libraries */
		const packages=fetch ('data/packages.json')
				.then (res => res.arrayBuffer())
				.then (buf => new TextDecoder().decode (buf))
				.then (JSON.parse)
				.then (packages => {
					sandbox.packages=packages;
					for (let name in sandbox.packages)
						sandbox.packages[name].loaded=null;
					sandbox.update_package_views();
				});

		/* Sandbox worker */
		let init_resolve=null;
		let init_reject=null;
		let init_promise=new Promise ((resolve,reject) => {
			init_resolve=resolve;
			init_reject=reject;
		});
		sandbox.worker=new Worker ('js/sandbox-worker.js');
		sandbox.worker.onmessage=function (e) {
			e=e.data;
			if ('stdout' in e)
				sandbox.stdout (e.stdout);
			if ('stderr' in e)
				sandbox.stderr (e.stderr);
			if ('flush_output' in e){
				sandbox.flush_output();
			}
			if ('init_done' in e){
				if (e.init_done.ok)
					init_resolve();
				else
					init_reject (e.init_done.error);
			}
			if ('add_package_done' in e)
				sandbox.add_package_callback (e.add_package_done);
			if ('compile_done' in e){
				sandbox.btn_compile.disabled=false;
				sandbox.compile_indicator.style.display='none';
				sandbox.btn_run.disabled=!e.compile_done.ok;
			}
		};
		sandbox.worker.postMessage ({init: {}});

		const stdenv=Promise.all ([packages, init_promise])
				.then (() => {
					let stdenv_versions=sandbox.packages['base-stdenv'];
					return sandbox.fetch_package ('base-stdenv',stdenv_versions[stdenv_versions.length-1]);
				});

		return Promise.all ([ui, packages, init_promise, stdenv])
				.then (() => {
					sandbox.btn_compile.disabled=false;
				});
	},
};

function create_monaco_editor (div,val)
{
	const monarch_definition={
		keywords: [
			'foreign', 'export', 'ccall', 'stdcall',
			'code', 'inline',
			'implementation', 'definition', 'system', 'module',
			'import', 'qualified', 'as', 'from', 'library',
			'infixl', 'infixr', 'infix',
			'if', 'otherwise',
			'let', 'in', 'case', 'of', 'with', 'where',
			'class', 'instance', 'special',
			'generic', 'derive',
			'dynamic'
		],

		// C# style strings
		escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,2}|0\d{0,2})/,

		brackets: [
			['{','}','delimiter.curly'],
			['[',']','delimiter.square'],
			['(',')','delimiter.parenthesis']
		],

		// The main tokenizer for our languages
		tokenizer: {
			root: [
				// identifiers and keywords
				[/[a-z_$][\w$]*/, {
					cases: {
						'@keywords': 'keyword',
						'@default': 'identifier'
					}
				}],
				[/[A-Z][\w\$]*/, {
					cases: {
						'Start': 'identifier',
						'@default': 'type.identifier'
					}
				}],

				// whitespace
				{ include: '@whitespace' },

				// list of characters shorthand (must be before brackets)
				[/\['/, { token: 'string.quote', bracket: '@open', next: '@charlist'} ],

				// delimiters and operators
				[/[{}()\[\]]/, '@brackets'],
				[/[;,.]/, 'delimiter'],

				// numbers
				[/\d+\.\d+([eE][\-+]?\d+)?/, 'number.float'],
				[/0[xX][0-9a-fA-F]+/, 'number.hex'],
				[/\d+/, 'number'],

				// strings
				[/"([^"\\]|\\.)*$/, 'string.invalid' ],  // non-teminated string
				[/"/,  { token: 'string.quote', bracket: '@open', next: '@string' } ],

				// characters
				[/'[^\\']'/, 'string'],
				[/(')(@escapes)(')/, ['string','string.escape','string']],
				[/'/, 'string.invalid']
			],

			comment: [
				[/[^\/*]+/, 'comment' ],
				[/\/\/.*/,  'comment'],
				[/\/\*/,    'comment', '@push'],
				["\\*/",    'comment', '@pop'],
				[/[\/*]/,   'comment' ]
			],

			string: [
				[/[^\\"]+/,  'string'],
				[/@escapes/, 'string.escape'],
				[/\\./,      'string.escape.invalid'],
				[/"/,        { token: 'string.quote', bracket: '@close', next: '@pop' } ]
			],

			charlist: [
				[/[^\\']+/,  'string'],
				[/@escapes/, 'string.escape'],
				[/\\./,      'string.escape.invalid'],
				[/']/,       { token: 'string.quote', bracket: '@close', next: '@pop' } ]
			],

			whitespace: [
				[/[ \t\r\n]+/, 'white'],
				[/\/\*/,       'comment', '@comment' ],
				[/\/\/.*$/,    'comment'],
			],
		},
	};

	const monarch_config={
		brackets: [
			['{','}'],
			['[',']'],
			['(',')']
		],
		surroundingPairs: [
			{open: '{', close: '}'},
			{open: '[', close: ']'},
			{open: '(', close: ')'},
			{open: '"', close: '"'},
			{open: "'", close: "'"},
		],
		autoClosingPairs: [
			{open: '{', close: '}'},
			{open: '[', close: ']'},
			{open: '(', close: ')'},
			{open: '"', close: '"'},
			{open: "'", close: "'"},
		]
	};

	return monaco_promise.then (() => {
		monaco.languages.register ({id: 'clean'});
		monaco.languages.setMonarchTokensProvider ('clean',monarch_definition);
		monaco.languages.setLanguageConfiguration ('clean',monarch_config);

		const editor=monaco.editor.create (div,{
			automaticLayout: true,
			detectIndentation: false,
			fontSize: 12,
			insertSpaces: false,
			language: 'clean',
			lineNumbersMinChars: 4,
			minimap: {enabled: false},
			mouseWheelZoom: true,
			renderWhitespace: 'selection',
			roundedSelection: false,
			showUnused: true,
			tabSize: 4,
			value: val
		});

		editor.addAction ({
			id: 'clean-sandbox-compile',
			contextMenuGroupId: '99_clean',
			label: 'Compile',
			keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_U],
			run: () => {
				sandbox.btn_compile.click();
			}
		});

		editor.addAction ({
			id: 'clean-sandbox-run',
			contextMenuGroupId: '99_clean',
			label: 'Run',
			keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.KEY_R],
			run: () => {
				if (sandbox.btn_run.disabled){
					window.alert ('Last compilation did not succeed; fix errors and recompile the program.');
				} else {
					sandbox.btn_run.click();
				}
			}
		});

		return editor;
	});
}
